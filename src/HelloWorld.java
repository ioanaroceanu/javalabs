public class HelloWorld {
    public int ceva = 20;
    public static void main(String args[]){
        String make = "Renault";
        String model = "Laguna";
        double engineSize = 1.8;
        byte gear = 2;
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        if(engineSize < 1.3){
            System.out.println("The make is weak");
        } else{
            System.out.println("The make is strong");
        }

        switch(gear){
            case 1: System.out.println("Suitable speed 20kmph");
            case 2: System.out.println("Suitable speed 40kmph");
            case 3: System.out.println("Suitable speed 60kmph");
            case 4: System.out.println("Suitable speed 80kmph");
            case 5: System.out.println("Suitable speed 100kmph");
        }

        for(int y=1900; y<2000; y+=4){
            System.out.println("Leap year "+ y);
            if(y == 1916){
                System.out.println("Finished");
                break;
            }
        }

        int[] myArray = new int[10];
        for(int y=1900; y<2000; y+=4){
            int index = (y-1900)/4;
            System.out.println(index);
            myArray[index] = y;
            if(y == 1936){
                System.out.println("Finished");
                break;
            }
        }

        for(int y : myArray){
            System.out.println(y);
        }

    }
}