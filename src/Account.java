public class Account implements Detailable{
    private double balance;
    private String name;

    public Account(int balance_, String name_) {
        balance = balance_;
        name = name_;
        System.out.println("Created Account");

    }

    public static void main(String args[]){
    }

    public double getBalance(){
        return this.balance;
    }

    public String getName(){
        return this.name;
    }

    public void setBalance(int balance_){
        this.balance = balance_;
    }

    public void setName(String name_){
        this.name = name_;
    }

    public void inceaseBalance(){
        this.balance += (this.balance*0.1);
    }

    public String getDetails(){
        String toReturn = "" + this.name + " " + this.balance;
        return toReturn;
    }
}
