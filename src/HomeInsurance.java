public class HomeInsurance implements Detailable{
    public int premium;
    public int excess;
    public int amountInsured;

    public HomeInsurance(int premium, int excess, int amountInsured){
        this.premium = premium;
        this.excess = excess;
        this.amountInsured = amountInsured;
    }

    public String getDetails(){
        String toReturn = "" + premium + " " + excess;
        return toReturn;
    }

}
