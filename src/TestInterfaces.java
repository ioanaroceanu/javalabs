public class TestInterfaces {
    public static void main(String args[]){
        Detailable[] products = new Detailable[3];

        Account d10 = new Account(1000, "Maria");
        HomeInsurance h30 = new HomeInsurance(20, 200, 2000);
        Account d90 = new Account(3000, "Anna");
        products[0] = d10;
        products[1] = h30;
        products[2] = d90;

        for(int i=0; i < products.length; i++){
            System.out.println(products[i].getDetails());
        }

    }
}
